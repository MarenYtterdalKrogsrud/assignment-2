# Assignment 2

File System Manager  

This project is a Java console application that makes it possible to manage and manipulate files.
The system has a main menu where the user can interact and choose between different options. 
Possible functionalities:
- listing files
- listing files by extension

The directory also contains a txt file where the program can use these functionalities:
- Print name of file
- File size
- How many lines the file has
- Search for a spesific word and see if it exists 
- Search for a word and see how many times it exists in the file
The searhes ignore case and the system contains input validation.

The system does also have a logging service where all the results from file manipulation is logged. 
This has a timestamp and a calculation of the duration of the function.

The resource folder contains the files that has been used in the project.

The jar file could not access the files and you therefor have to download the project to run it.
