package loggingService;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggingService {

    //this method is creating a logging service for all the different methods
    //
    public static void logMessage(String message, long systemTime) {
        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;
        try {
            // This block configure the logger with handler and formatter
            fh = new FileHandler("src/loggingService/myLog.log", true);
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            // the following statement is used to log any messages
            if (systemTime == 0) { // if systemTime is not added
                logger.info(message);
            } else { //Prints the time, function call and duration for each function
                logger.info(message + ". The function took " + (System.currentTimeMillis() - systemTime) + "ms to execute.");
            }
            // log to console
            System.out.println(message);

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }
}
