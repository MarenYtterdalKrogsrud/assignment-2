package system;

import loggingService.LoggingService;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.*;
import java.util.Scanner;

public class FileInfo {

    //This method gets the file name of the txt file
    public static void getFileName(){
        long systemTime;  //starts calculating duration time for function
        systemTime = System.currentTimeMillis();
        File f = new File("src/resources/Dracula.txt");
        LoggingService.logMessage(f.getName(), systemTime); //???
    }

    //This method is calculating the txt file size by using the FileInputStream which reads the raw bytes.
    //With a while loop it counts amount of bytes and calculates the bytes into kilobytes
    public static void TxtFileSize() {
        long systemTime;
        systemTime = System.currentTimeMillis();
        try (FileInputStream fileSize = new FileInputStream("src/resources/Dracula.txt")) {

            int data = fileSize.read();
            int byteCounter = 0;
            while (data != -1) {
                byteCounter++;
                data = fileSize.read();
            }
            double kiloBytes = byteCounter / 1024.0; //calculating from bytes to KB
            LoggingService.logMessage("The file is " + byteCounter + " bytes, or " + kiloBytes + " KB", systemTime);

        } catch (IOException ex) { //general class of exceptions
            System.out.println("Something went wrong. The system could not calculate the file size." + ex.getMessage());
        }
    }

    // This method calculates the number of lines in the Dracula.txt file by using the bufferedReader.
    // BufferedReader reads each line of the txt file and then adds the number to the variable
    public static void CalculateTxtLines() {
        long systemTime;
        systemTime = System.currentTimeMillis();
        long linesNumber = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader("src/resources/Dracula.txt"))) {
            while (reader.readLine() != null) linesNumber++;
        } catch (IOException e) {
            System.out.println("Something went wrong. The system could not calculate number of lines." + e.getMessage());
        }
             LoggingService.logMessage("The number of lines in the Dracula txt file: " + linesNumber, systemTime);
    }

    // This method takes the users input from scanner and read the file with bufferedReader.
    // With a while loop it checks if the word is present in the file and the counter counts the amount of times
    // it is present in the file and prints it to the user
    public static void searchWordCount() {
        long systemTime;
        systemTime = System.currentTimeMillis();
        try {
            File file1 = new File("src/resources/Dracula.txt"); //Creation of File Descriptor for input file
            String[] words;
            FileReader fileR = new FileReader(file1);
            BufferedReader reads = new BufferedReader(fileR);

            Scanner scanner = new Scanner(System.in); //creates the scanner
            System.out.println("Please enter the word you want to search for in the file: ");
            String search;
            String inputWord = scanner.nextLine();  // creates a variable for the scanner input
            int count = 0;
            while ((search = reads.readLine()) != null){
                words = search.split("[\\s@&.,?+-]+");  //To only get the exact word and to avoid problems with special characters
                for (String word : words) {
                    if (word.equalsIgnoreCase(inputWord)){  //ignore case
                        count++;
                    }
                }
            }

            //feedback if the word is present or not.
            if (count != 0){
                LoggingService.logMessage("The word " + inputWord + " is present " + count + " times in the txt file", systemTime);
            }
            if (count == 0) {
                LoggingService.logMessage("Sorry, the given word you wrote is not present in the file.", systemTime);
            }
            fileR.close();
        } catch (IOException ex) {
            System.out.println("Something went wrong" + ex);
        }
    }
       //This method takes the users input from scanner and read the file with bufferedReader.
       //Then it checks if the word exists and prints the information to the user.
        public static void searchWord() {
            long systemTime;
            systemTime = System.currentTimeMillis();

            try {
                File file2 = new File("src/resources/Dracula.txt"); //Creation of File Descriptor for input file
                String[] words;
                FileReader fileR = new FileReader(file2);
                BufferedReader reads = new BufferedReader(fileR);

                Scanner scanner = new Scanner(System.in); //creates the scanner
                System.out.println("Please enter the word you want to search for in the file: ");
                String search;
                String inputWord = scanner.nextLine();  // creates a variable for the scanner input
                while ((search = reads.readLine()) != null){
                    words = search.split("[\\s@&.,?+-]+");  //To only get the exact word and to avoid problems with special characters
                    for (String word : words) {
                        if (word.equalsIgnoreCase(inputWord)){  //ignore case
                            LoggingService.logMessage("Yes, the word exist", systemTime);
                            return;
                        }
                    }
                }
                LoggingService.logMessage("No, the word does not exists", systemTime);
                fileR.close();
            } catch (IOException ex) {
                System.out.println("Something went wrong" + ex);
            }
        }
}
