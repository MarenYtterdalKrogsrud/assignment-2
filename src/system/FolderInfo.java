package system;

import loggingService.LoggingService;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;

public class FolderInfo {

    //this method lists all the different files in the resource folder
    public static void listFileNames() {
        String dir = "src/resources";
        long systemTime;
        systemTime = System.currentTimeMillis(); //starts calculating duration time for function
        try {
            Files.list(new File(dir).toPath())      //loops through the folder in a path and prints the information
                    .limit(15)                      //sets the limit to 15 as there is only a few files in the folder
                    .forEach(path -> {
                        System.out.println();
                        LoggingService.logMessage(path.toString(), systemTime);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //This method takes the input from the fileExtensionMenu method to get
    // the list of files with the particular extention
    public static void listExtensionFiles(String input) {
        long systemTime;
        systemTime = System.currentTimeMillis(); //starts calculating duration time for function

        String fileType;
        if(input.charAt(0) != '.') {   //add a dot in case the user forgot to add it
            fileType = '.' + input;
        } else {
            fileType = input;
        }

        File dir2 = new File("src/resources");
        FilenameFilter filefilter = new FilenameFilter() { //filter filenames
            public boolean accept(File dir2, String name) {
                String lowercaseName = name.toLowerCase(); //creates them to lowercase
                return lowercaseName.endsWith(fileType.toLowerCase());  //condition if the file ends with txt
            }
        };
        String[] filesList = dir2.list(filefilter); //creates a list of the files with the chosen extension
        if(filesList.length == 0) { //check if there exists any extension
            LoggingService.logMessage("Did not find any files with extension " + fileType, systemTime);
            return;
        }

        System.out.println("The list of the " + fileType + " files in the directory: ");

        for (String fileName : filesList) {
            LoggingService.logMessage(fileName, systemTime); //list the files
        }
    }
}