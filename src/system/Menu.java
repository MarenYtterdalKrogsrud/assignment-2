package system;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {

    // This method creates the main menu in the program. It lists all the different options to the user
    // and makes it possible for them to type it in. The input is also validated by a switch case
    // and an if statement.
    public static void menuSetting(){
            Scanner scan = new Scanner(System.in);
            System.out.println("____________________________________________");
            System.out.println("Please choose one of the following options: ");
            System.out.println("1) Show all files");
            System.out.println("2) Get files by extension");
            System.out.println("3) Manipulate text file");
            System.out.println("4) Quit the application");
            System.out.print("Selection: ");

            if(!scan.hasNextInt()){   // repromt user if the input is invalid
                System.out.println("Invalid input.");
                return;
            }

            int option = scan.nextInt();

            // Switch case for the options that calls the different methods.
            switch (option) {
                case 1:
                    System.out.println("Here are all the files in the directory: ");
                    FolderInfo.listFileNames(); //call method
                    break;
                case 2:
                    System.out.println("List of the file extensions: ");
                    fileExtensionMenu();
                    break;
                case 3:
                    System.out.println("File manipulation: ");
                    fileManipulationMenu();
                    break;
                case 4:
                    System.out.println("Exit selected");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid input. ");
                    break;
            }
    }

            //This method is to list all the different file extensions
            public static void fileExtensionMenu() {

                File dir2 = new File("src/resources");
                String[] filesList = dir2.list();  //creates an array of files
                List listOfExtensions = new ArrayList();
                for (String fileName : filesList) {
                    String extension = "." + fileName.substring(fileName.lastIndexOf(".") + 1); //gets the extension of the file
                    if (!listOfExtensions.contains(extension)) { //adds to a list if it is not already added
                        listOfExtensions.add(extension);
                    }
                }
                for (Object extension : listOfExtensions) {
                    System.out.println(extension);
                }
                //create a scanner for the user to type in the extension
                Scanner scan3 = new Scanner(System.in);
                System.out.println("Type in the extension you want to see:");

                String option2 = scan3.nextLine();
                FolderInfo.listExtensionFiles(option2);

            }

                //This method creates a submenu (for the file manipulation)
                //Similar to the main menu where a switch case has been made and methods are called
                public static void fileManipulationMenu(){

                    Scanner scan2 = new Scanner(System.in);

                    System.out.println("Please choose one of the following options: ");
                    System.out.println("1) Display name of file ");
                    System.out.println("2) Display file size ");
                    System.out.println("3) Display number of lines in file ");
                    System.out.println("4) Search for word in file");
                    System.out.println("5) Search for word in file and check number of occurrences ");
                    System.out.print("Selection: ");

                    if(!scan2.hasNextInt()){   //this is to check if the input is not a number before the switch case
                        System.out.println("Invalid input.");
                        return;
                    }

                    int option = scan2.nextInt();

                    switch (option) {
                        case 1:
                            FileInfo.getFileName();
                            break;
                        case 2:
                            FileInfo.TxtFileSize();
                            break;
                        case 3:
                            FileInfo.CalculateTxtLines();
                            break;
                        case 4:
                            FileInfo.searchWord();
                            break;
                        case 5:
                            FileInfo.searchWordCount();
                            break;
                        default:
                            System.out.println("Invalid selection you need to type one of the numbers above");
                            break;
                    }
                }

}
